﻿# Studenti, kteří splnili požadavky k zápočtu

| Jméno        | Příjmení           | Osobní číslo  |
| ------------- |-------------| -----|
| Dan      | Charousek | F15001 |
| Marek    | Beran     | ST75216 |
| Jiří     | Machovec  | F16057 |
| Jan | Zaloudek | F17031 |
| Jakub | Slabý | F17025 |
| Miroslav | Douda | F16382 |
| Jiří | Mít | F17022 |
| Pavel      | Jahoda | F17012 |
| Lukáš | Juřica | ST77215
| Karyna   | Kulish    | F17003 |
| Sára     | Tesaříková| F17028 |
| Tereza | Jozífková | F17014 |
| Vladimir | Benes | F17006 |
| Václav | Kříž | F17020 |
